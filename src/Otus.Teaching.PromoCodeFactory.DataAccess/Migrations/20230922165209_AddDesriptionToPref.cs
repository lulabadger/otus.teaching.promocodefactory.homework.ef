﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Migrations
{
    /// <inheritdoc />
    public partial class AddDesriptionToPref : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Description",
                table: "Preferences",
                type: "TEXT",
                maxLength: 150,
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                column: "Description",
                value: null);

            migrationBuilder.UpdateData(
                table: "Preferences",
                keyColumn: "Id",
                keyValue: new Guid("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                column: "Description",
                value: null);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Description",
                table: "Preferences");
        }
    }
}

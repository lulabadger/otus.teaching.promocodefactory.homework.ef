﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmployeeRepository : EfRepository<Employee>
    {        
        public EmployeeRepository(DatabaseContext dbContext):base (dbContext)
        {            
        }

        public override Task<Employee> GetByIdAsync(Guid id)
        {
            return _entitySet.Include(e => e.Role).FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{

    public class CustomerRepository : EfRepository<Customer>
    {
       
        public CustomerRepository(DatabaseContext dbContext) : base(dbContext)
        {
        }

        public override Task<Customer> GetByIdAsync(Guid id)
        {
            return _entitySet.Include(e => e.PromoCodes).Include(e => e.Preferences).FirstOrDefaultAsync(x => x.Id == id);
        }

        public override Task<Customer> GetByPreferenceIdAsync(Guid preferenceId)
        {
            return _entitySet
                        .Include(e => e.CustomerPreferences)
                       .FirstOrDefaultAsync(x => x.CustomerPreferences.Any(x => x.PreferenceId == preferenceId));
        }
     
    }
}

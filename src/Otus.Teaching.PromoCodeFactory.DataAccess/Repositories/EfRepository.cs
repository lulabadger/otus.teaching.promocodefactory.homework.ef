﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T>
        where T : BaseEntity
    {
        protected readonly DatabaseContext _dbContext;
        protected readonly DbSet<T> _entitySet;

        public EfRepository(DatabaseContext dbContext)
        {
            _dbContext = dbContext;
            _entitySet = _dbContext.Set<T>();
        }

        public virtual Task<bool> CreateAsync(T entity)
        {
            if (entity != null)
            {
                this._entitySet.Add(entity);
                this._dbContext.SaveChanges();
                return Task.FromResult(true);
            }
            else
            {
                return Task.FromResult(false);
            }
        }

       

        public virtual Task<bool> DeleteAsync(Guid id)
        {
            var entity = this.GetByIdAsync(id);
            if (entity != null)
            {
                this._entitySet.Remove(entity.Result);
                this._dbContext.SaveChanges();
                return Task.FromResult(true);
            }
            else
            {
                return Task.FromResult(false);
            }
        }

        public virtual Task<List<T>> GetAllAsync()
        {
            return Task.FromResult(_entitySet.ToList());
        }

        public virtual Task<T> GetByIdAsync(Guid id)
        {
            return _entitySet.FirstOrDefaultAsync(x => x.Id == id);
        }

        public Task<T> UpdateAsync(T entity)
        {
            if (entity != null)
            {

                this._entitySet.Update(entity);
                this._dbContext.SaveChanges();
            }
            return Task.FromResult(entity);
        }

      

        public virtual Task<T> GetByPreferenceIdAsync(Guid preferenceId)
        {
            throw new NotImplementedException();
        }
    }
}

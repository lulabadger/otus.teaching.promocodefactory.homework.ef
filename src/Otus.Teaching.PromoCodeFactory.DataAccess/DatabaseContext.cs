﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext(DbContextOptions<DatabaseContext> options)
            : base(options)
        {
            //Database.EnsureDeleted();
           // Database.EnsureCreated();
            Database.Migrate();
        }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Preference> Preferences { get; set; }
        public DbSet<CustomerPreference> CustomerPreferences { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Employee>().HasOne(e => e.Role)
               .WithMany(e => e.Employees)
                .HasForeignKey(e => e.RoleId);

            modelBuilder.Entity<CustomerPreference>().HasOne(e => e.Customer)
               .WithMany(e => e.CustomerPreferences)
               .HasForeignKey(e => e.CustomerId);

            modelBuilder.Entity<CustomerPreference>().HasOne(e => e.Preference)
                .WithMany(e => e.CustomerPreferences)
                .HasForeignKey(e => e.PreferenceId);



            modelBuilder.Entity<Customer>()
                    .HasMany(e => e.Preferences)
                    .WithMany(e => e.Customers)
                    .UsingEntity<CustomerPreference>();

            modelBuilder.Entity<PromoCode>().HasOne(e => e.Preference)
                .WithMany(e => e.PromoCodes)
                .HasForeignKey(e => e.PreferenceId);

            modelBuilder.Entity<PromoCode>().HasOne(e => e.PartnerManager)
                .WithMany(e => e.PromoCodes)
                .HasForeignKey(e => e.PartnerManagerId);

            modelBuilder.Entity<PromoCode>().HasOne(e => e.Customer)
                .WithMany(e => e.PromoCodes)
                .HasForeignKey(e => e.CustomerId);

            modelBuilder.Entity<Role>().Property(c => c.Name).HasMaxLength(100);
            modelBuilder.Entity<Role>().Property(c => c.Description).HasMaxLength(255);


            modelBuilder.Entity<Employee>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Employee>().Property(c => c.Email).HasMaxLength(320);
            modelBuilder.Entity<Preference>().Property(c => c.Name).HasMaxLength(100);
            modelBuilder.Entity<Preference>().Property(c => c.Description).HasMaxLength(150);

            modelBuilder.Entity<Customer>().Property(c => c.FirstName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.LastName).HasMaxLength(100);
            modelBuilder.Entity<Customer>().Property(c => c.Email).HasMaxLength(320);


            modelBuilder.Entity<PromoCode>().Property(c => c.Code).HasMaxLength(50);
            modelBuilder.Entity<PromoCode>().Property(c => c.ServiceInfo).HasMaxLength(255);
            modelBuilder.Entity<PromoCode>().Property(c => c.PartnerName).HasMaxLength(100);


            modelBuilder.Entity<Role>().HasData(FakeDataFactory.Roles);  
            modelBuilder.Entity<Employee>().HasData(FakeDataFactory.Employees);

            modelBuilder.Entity<Preference>().HasData(FakeDataFactory.Preferences);          
            modelBuilder.Entity<Customer>().HasData(FakeDataFactory.Customers);
            modelBuilder.Entity<CustomerPreference>().HasData(FakeDataFactory.CustomerPreferences);
            modelBuilder.Entity<PromoCode>().HasData(FakeDataFactory.PromoCodes);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }
    }
}

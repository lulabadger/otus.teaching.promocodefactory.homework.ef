﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
        where T: BaseEntity
    {
        Task<List<T>> GetAllAsync();
        
        Task<T> GetByIdAsync(Guid id);

        Task<bool> CreateAsync(T entity);
        
     
        Task<T> UpdateAsync(T entity);

        Task<bool> DeleteAsync(Guid id);

        Task<T> GetByPreferenceIdAsync(Guid preferenceId);
    }
}
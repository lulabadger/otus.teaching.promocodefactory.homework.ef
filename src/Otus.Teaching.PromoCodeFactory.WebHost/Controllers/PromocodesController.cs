﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Metadata.Ecma335;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Namotion.Reflection;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {

        private readonly IRepository<PromoCode> _promocodeRepository;
        private readonly IRepository<Customer> _сustomerRepository;
        private readonly IRepository<Employee> _managerRepository;

        public PromocodesController(IRepository<PromoCode> promocodeRepository, IRepository<Customer> сustomersRepository,
            IRepository<Employee> employeeRepository )
        {
            _promocodeRepository = promocodeRepository;
            _сustomerRepository = сustomersRepository;
            _managerRepository = employeeRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var promocodes = await _promocodeRepository.GetAllAsync();
            var promocodModelList = promocodes.Select(x => new PromoCodeShortResponse() {                  
                Id =x.Id,
                Code=x.Code,
                BeginDate=x.BeginDate.ToString("dd.MM.yyyy"),
                EndDate=x.EndDate.ToString("dd.MM.yyyy"),
                PartnerName=x.PartnerName,
                ServiceInfo=x.ServiceInfo
            }).ToList();
            return promocodModelList;
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {

            if (request == null) return BadRequest();
            var manager = await _managerRepository.GetByIdAsync(request.PartnerManagerId);
            if (manager == null)
                return NotFound("Не найден PartnerManager");

            var сustomer = await _сustomerRepository.GetByPreferenceIdAsync(request.PreferenceId);
            if (сustomer == null)
                return NotFound("Не найдены клиенты с выбранными предпочтениями");
            
         

            PromoCode entity = new PromoCode()
            {
                Code = request.PromoCode,
                PartnerName = request.PartnerName,
                ServiceInfo = request.ServiceInfo,
                PartnerManagerId = request.PartnerManagerId,
                PreferenceId=request.PreferenceId,
                BeginDate=DateTime.Now,
                EndDate=DateTime.Now.AddMonths(1),
                CustomerId= сustomer.Id
            };


           var result= await this._promocodeRepository.CreateAsync(entity);
           return (result) ? Ok("Создан новый промокод и выдан клиенту") : BadRequest();
        }
    }
}
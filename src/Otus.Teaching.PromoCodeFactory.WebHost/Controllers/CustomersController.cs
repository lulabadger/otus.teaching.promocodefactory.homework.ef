﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _customerRepository;

        public CustomersController(IRepository<Customer> customerRepository)
        {
            this._customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить данные всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<CustomerShortResponse>> GetCustomersAsync()
        {
           var customers = await _customerRepository.GetAllAsync();
           var customerModelList= customers.Select(x => new CustomerShortResponse() 
           {
               Id=x.Id,
               Email=x.Email,
               FirstName=x.FirstName,
               LastName=x.LastName           
           
           }).ToList();
           return customerModelList;
        }

        /// <summary>
        /// Получить данные клиента по id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customerRepository.GetByIdAsync(id);
            if (customer == null)
                return NotFound();

            return new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                PromoCodes = customer.PromoCodes?.Select(c =>
                {
                    return new PromoCodeShortResponse()
                    {
                        Id = c.Id,
                        BeginDate = c.BeginDate.ToShortDateString(),
                        EndDate = c.EndDate.ToShortDateString(),
                        Code = c.Code,
                        PartnerName = c.PartnerName,
                        ServiceInfo = c.ServiceInfo
                    };
                }).ToList(),
                Preferences = customer.Preferences?.Select(k => 
                {
                    return new PreferenceResponse()
                    {
                        Id = k.Id,
                        Name = k.Name
                    };
                }).ToList()
            };
        }
        /// <summary>
        /// Создать нового клиента вместе с предпочтениями
        /// </summary>
        [HttpPost]
        public async Task<ActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            if (request ==null) return BadRequest();

            var entity = new Customer
            {   
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email,
                CustomerPreferences = request.PreferenceIds.Select(id => new CustomerPreference
                {
                    PreferenceId = id
                }).ToList()
            };
            await _customerRepository.CreateAsync(entity);
           return Ok("Создан новый объект");
        }
        
        /// <summary>
        /// Обновить данные клиента вместе с его предпочтениями по id
        /// </summary>        
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            if (request == null) return BadRequest();

            var сustomer = await _customerRepository.GetByIdAsync(id);
            if (сustomer == null)
                return NotFound("Клиент не найден");

            сustomer.Email = request.Email;
            сustomer.FirstName = request.FirstName;
            сustomer.LastName = request.LastName;
            сustomer.CustomerPreferences = request.PreferenceIds.Select(id => new CustomerPreference
            {
                PreferenceId = id
            }).ToList();

            await _customerRepository.UpdateAsync(сustomer);

            return Ok("Данные клиента обновлены вместе с его предпочтениями");
        }


        /// <summary>
        /// Удалить клиента вместе с выданными ему промокодами по id
        /// </summary> 
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {   
            var result = await _customerRepository.DeleteAsync(id);
            return (result) ? Ok("Объект удален") : NotFound("Клиент не найден");
        }


    }
}